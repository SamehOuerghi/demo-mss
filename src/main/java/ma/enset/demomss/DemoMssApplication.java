package ma.enset.demomss;

import ma.enset.demomss.WebServiceRest.ProduitRestRepository;
import ma.enset.demomss.entities.Produit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class DemoMssApplication {

    public static void main(String[] args) {

        SpringApplication.run(DemoMssApplication.class, args);

    }
/*
On utilise @Bean pour que la methode s"execute au demarrage
* */
   @Bean
  CommandLineRunner start(ProduitRestRepository produitRepository, RepositoryRestConfiguration restConfiguration){
        return args->{
            restConfiguration.exposeIdsFor(Produit.class);
            produitRepository.save(new Produit(null,"Ord HP 54",6000,3));
            produitRepository.save(new Produit(null,"Imprimante Epson ",2000,13));
            produitRepository.save(new Produit(null,"Smartphone",300,23));
            produitRepository.findAll().forEach(p->{
                System.out.println(p.getName());
            });

        };
  }

}
